1. I utilized React and Gatsby (similar to Vue and Angular but chose
it because of the way it can pair with GraphQL) as the modern
front-end framework, with Bootstrap
2. Focused on the responsive sidenav and the topnav
3. Utilized only FontAwesome fonts
4. Utilized GraphQL to handle additional job descriptions and storing
and accessing the job description information

I ended up time-boxing about 4 hours, part of it was taking time to
figure oput how to archtiect the page where the navigation works, and
how the user can go between the top-main nav, and the left nav.

If I had more time I would:

* Update fonts
* slim down dependencies
* Instantiate the Tabs in the content area
* finish the other jobs layout section
* secure to match the outline icon styles