import { graphql } from 'gatsby'
import Img, { FixedObject } from 'gatsby-image'
import React from 'react'

import { siteMetadata } from '../../gatsby-config'
import Layout from '../components/layout/layout'
import Meta from '../components/meta/meta'

interface Props {
  location: Location
}

const Questions: React.FC<Props> = ({ location }: Props) => {

  return (
    <Layout location={location}>
      <Meta site={siteMetadata} title="Questions" />
      <div>
        <section className="text-center">
          <div className="container">
            <h1>Questions</h1>
          </div>
        </section>
      </div>
    </Layout>
  )
}

export default Questions
