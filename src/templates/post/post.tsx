import { Link } from 'gatsby'
import Img, { FluidObject } from 'gatsby-image'
import React from 'react'

import Adsense from '../../components/adsense/adsense'
import Button from '../../components/button/button'
import Badge from '../../components/badge/badge'
import Card from '../../../'
import { PostByPathQuery } from '../../../types/graphql-types'

import './style.scss'

const getDescription = (content: string): string => {
  const body = content.replace(
    /<blockquote>/g,
    '<blockquote class="blockquote">'
  )
  if (body.match('<!--more-->')) {
    const [description] = body.split('<!--more-->')
    return description
  }
  return body
}

interface Props {
  data: PostByPathQuery
  options: {
    isIndex: boolean
    adsense?: string | null
  }
}

const Post: React.FC<Props> = ({ data, options }: Props) => {
  const frontmatter = data.post?.frontmatter
  const path = frontmatter?.path || ''
  const image = frontmatter?.image || null
  const { isIndex, adsense } = options
  const html = data.post?.html || ''

  return (
    <div className="article" key={path}>
      <div className="container-fluid">
        <div className="top-section">
            <h1>{frontmatter?.rank}</h1>
            <div>{frontmatter?.shipType}</div>
        </div>
        <div className="row">
          <div className="col-lg">
            <div className="content card">
              <h2>Job Description</h2>
              <table className="table">
                <tbody>
                <tr>
                  <td>Rank</td>
                  <td>{frontmatter?.rank}</td>
                </tr>
                <tr>
                  <td>Ship type</td>
                  <td>{frontmatter?.shipType}</td>
                </tr>
                <tr>
                  <td>Min. Experience</td>
                  <td>{frontmatter?.minExperience}</td>
                </tr>
                <tr>
                  <td>US Visa</td>
                  <td>Required</td>
                </tr>
                <tr>
                  <td>Approx Joining</td>
                  <td>{frontmatter?.date}</td>
                </tr>
                <tr>
                  <td>Salary</td>
                  <td>{frontmatter?.minExperience}</td>
                </tr>
                <tr>
                  <td>Ship Details</td>
                  <td>{frontmatter?.shipDetails}</td>
                </tr>
                <tr>
                  <td>Description</td>
                  <td>{frontmatter?.description}</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="col-lg col-lg-4">
          <div className="content card">
            <h3>Share on facebook</h3>
            </div>
            <div className="content card">
              <h2>Other Jobs</h2>

            </div>
          </div>
        </div>
      </div>
      
    </div>
  )
}

export default Post
