import React from 'react'
import { Link } from 'gatsby'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog, faMailBulk, faEnvelopeOpenText, faSearch, faUserAlt } from '@fortawesome/free-solid-svg-icons'

interface Props {
  title: string
  location: Location
}

/* custom theming */
import './style.scss';

const Navibar: React.FC<Props> = ({ location, title }: Props) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light navbar-theme-pos sticky-top">
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#leftNav" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle left navigation">
          <FontAwesomeIcon icon={faUserAlt}></FontAwesomeIcon>
      </button>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#topNav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="topNav">
        {/* <Link className="text-center" to="/">
          <h1 className="navbar-brand mb-0">{title}</h1>
        </Link> */}
        <ul className="navbar-nav bd-navbar-nav">
          <li className="icon"><FontAwesomeIcon icon={faSearch}></FontAwesomeIcon></li>
        </ul>
          <ul className="navbar-nav bd-navbar-nav">
            <li
              className={
                location.pathname === '/' ? 'nav-item active' : 'nav-item'
              }
            >
              <Link to="/" className="nav-link">
                Blog
              </Link>
            </li>
            <li
              className={
                location.pathname === '/questions/'
                  ? 'nav-item active'
                  : 'nav-item'
              }
            >
              <Link to="/questions/" className="nav-link">
                Questions
              </Link>
            </li>
            <li
              className={
                location.pathname === '/companies/'
                  ? 'nav-item active'
                  : 'nav-item'
              }
            >
              <Link to="/companies/" className="nav-link">
                Companies
              </Link>
            </li>
            <li
              className={
                location.pathname === '/contact/'
                  ? 'nav-item active'
                  : 'nav-item'
              }
            >
              <Link to="/contact/" className="nav-link">
                Contact
              </Link>
            </li>
          </ul>
          <ul className="navbar-nav bd-navbar-nav">
            <li className="icon">
               <FontAwesomeIcon icon={faCog}></FontAwesomeIcon>
            </li>
            <li className="icon">
               <FontAwesomeIcon icon={faMailBulk}></FontAwesomeIcon>
            </li>
            <li className="icon">
               <FontAwesomeIcon icon={faEnvelopeOpenText}></FontAwesomeIcon>
            </li>
          </ul>
        </div>
    </nav>
  )
}

export default Navibar
