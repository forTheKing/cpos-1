import React, { useEffect } from 'react'
import emergence from 'emergence.js'

import Button from '../../components/button/button'
import Navibar from '../navibar/navibar'
import Footer from '../footer/footer'
import { siteMetadata } from '../../../gatsby-config'

import 'modern-normalize/modern-normalize.css'
import 'prismjs/themes/prism.css'
import 'scss/gatstrap.scss'
import 'animate.css/animate.css'
import 'font-awesome/css/font-awesome.css'

interface Props {
  children?: React.ReactNode
  location: Location
}

import './style.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLaptop, faIndustry, faFileAlt, faUser } from '@fortawesome/free-solid-svg-icons'

const Layout: React.FC<Props> = ({ children, location }: Props) => {
  useEffect(() => {
    emergence.init()
  })

  return (
    <div className="container-fluid">
      <div className="row">
        <div id="leftNav" className="drawer col-lg col-lg-3 no-gutters collapse collapse-hidden">
            <div className="image-container">
              <div className="profileImage">
              </div>
            </div>
            <div className="text-center">
                  <h5>Devindar Kumar</h5>
                  <p>Chief office</p>
                  <Button label="Update Profile" path="#" primary></Button>
            </div>
            <ul className="leftNav">
              <li className="leftNav-item">
                <FontAwesomeIcon icon={faLaptop}></FontAwesomeIcon>
                <div>
                  Dashboard
                </div>
              </li>
              <li className="leftNav-item active">
              <FontAwesomeIcon icon={faIndustry}></FontAwesomeIcon>
                <div>
                  Jobs
                </div>
              </li>
              <li className="leftNav-item">
              <FontAwesomeIcon icon={faFileAlt}></FontAwesomeIcon>
                <div>
                  Sea docs
                </div>
              </li>
              <li className="leftNav-item">
              <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
                <div>
                  Connections
                </div>
              </li>
            </ul>
        </div>
        <div className="col-lg no-gutters">
          <Navibar title={siteMetadata.title} location={location} />
          {children}
          <Footer title={siteMetadata.title} author={siteMetadata.author} />
        </div>
      </div>
    </div>
  )
}

export default Layout
