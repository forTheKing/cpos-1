---
rank: 'Master'
shipType: 'Chemical Tanker Example 2'
minExperience: '06 Months'
date: '20 September 2012'
salary: '10000 USD'
shipDetails: 'Chembulk gibrlater, 2000 DWT, 2010 Built'
layout: post
path: '/hello-world/'
description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis excepturi, sequi pariatur numquam facilis atque perferendis quia ea suscipit assumenda enim velit, omnis inventore repellendus fugit consequuntur ducimus eos! Est?'
---

Paragraphs are separated by a blank line.

2nd paragraph. _Italic_, **bold**, and `monospace`. Itemized lists
look like:

- this one
- that one
- the other one

## An h2 header

content starts at 4-columns in.

> Block quotes are
> written like so.
>
> They can span multiple paragraphs,
> if you like.

Use 3 dashes for an em-dash. Use 2 dashes for ranges (ex., "it's all
in chapters 12--14"). Three dots ... will be converted to an ellipsis.
Unicode is supported 😄

```js
// javascript
console.log('Hello World!!')
```
