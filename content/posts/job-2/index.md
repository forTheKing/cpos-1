---
rank: 'Another Job'
shipType: 'Job read by gatsby'
minExperience: '06 Months'
date: '20 September 2016'
salary: '5000 USD'
shipDetails: 'Chembulk gibrlater, 2000 DWT, 2010 Built'
layout: post
path: '/hello-world/'
description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis excepturi, sequi pariatur numquam facilis atque perferendis quia ea suscipit assumenda enim velit, omnis inventore repellendus fugit consequuntur ducimus eos! Est?'
---

Wow! I love blogging so much already.

Did you know that "despite its name, salted duck eggs can also be made from chicken eggs, though the taste and texture will be somewhat different, and the egg yolk will be less rich."? ([Wikipedia Link](http://en.wikipedia.org/wiki/Salted_duck_egg))

Yeah, I didn't either.
